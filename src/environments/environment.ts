// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCnV_50KFZRp17zLF6pj9Z7On7eQPc7Ktw",
    authDomain: "college-enquiry-system-jrudjf.firebaseapp.com",
    databaseURL: "https://college-enquiry-system-jrudjf.firebaseio.com",
    projectId: "college-enquiry-system-jrudjf",
    storageBucket: "college-enquiry-system-jrudjf.appspot.com",
    messagingSenderId: "57304777413",
    appId: "1:57304777413:web:6d6380e3d4b0934b46a6ac"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
