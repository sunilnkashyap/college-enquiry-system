import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user;
  constructor(public afAuth: AngularFireAuth, public router: Router) {
    this.afAuth.user.subscribe(x => {
      console.log(x);
      this.user = x;
    });
  }

  ngOnInit() {

  }

  logout() {
    this.afAuth.auth.signOut().then(x => {
      this.router.navigateByUrl('/auth/login');
    }).catch(e => {
      alert('Something went wrong - ' + e.message)
    })
  }
}
