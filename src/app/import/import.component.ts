import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { staff, students, teacher } from '../../dataset/users';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, private afs: AngularFirestore) { }

  ngOnInit() {
    console.log(staff, students, teacher);
  }

  import(role) {

    let dataSource = [];

    switch (role) {
      case 'Student':
        dataSource = students;
        break;
      case 'Teacher':
        dataSource = teacher;
        break;
      case 'Staff':
        dataSource = staff;
        break;
    }

    dataSource.map((x: any) => {
      this.afAuth.auth.fetchSignInMethodsForEmail(x.email).then(d => {
        console.log(d);
        if (d.length > 0) {
          this.saveUserData(x, role);
        } else {
          this.createUser(x.email).then((g) => {
            this.saveUserData(x, role);
          });
        }
      }).catch(e => {
        console.log(e);
      });
    });

  }

  createUser(email) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, '123456');
  }

  saveUserData(data, role) {
    const userData = {
      email: data.email,
      name: data.name,
      role,
      user_id: Math.floor(1000 + Math.random() * 9999),
      mobile: data.mobile,
      department: data.department
    };

    const itemsCollection = this.afs.collection('users');
    itemsCollection.doc(data.email).set(userData).then(p => {
      console.log(p);
      if (role === 'Student') {
        this.saveStudentAttendance(data.email);
        this.saveStudentMarks(data.email);
      } else {
        this.saveSalaries(data.email);
      }
    }).catch((e) => {
      console.log('error save data', e)
    })
  }

  saveSalaries(email) {
    const data = {
      January: Math.floor(5000 + Math.random() * 50000),
      February: Math.floor(5000 + Math.random() * 50000),
      March: Math.floor(5000 + Math.random() * 50000),
      April: Math.floor(5000 + Math.random() * 50000),
      May: Math.floor(5000 + Math.random() * 50000),
      June: Math.floor(5000 + Math.random() * 50000),
      July: Math.floor(5000 + Math.random() * 50000),
      August: Math.floor(5000 + Math.random() * 50000),
      September: Math.floor(5000 + Math.random() * 50000),
      October: Math.floor(5000 + Math.random() * 50000),
      November: Math.floor(5000 + Math.random() * 50000),
      December: Math.floor(5000 + Math.random() * 50000),
    };
    const itemsCollection = this.afs.collection('salaries');
    itemsCollection.doc(email).set(data);
  }

  saveStudentAttendance(email) {
    const data = {
      January: Math.floor(50 + Math.random() * 99),
      February: Math.floor(50 + Math.random() * 99),
      March: Math.floor(50 + Math.random() * 99),
      April: Math.floor(50 + Math.random() * 99),
      May: Math.floor(50 + Math.random() * 99),
      June: Math.floor(50 + Math.random() * 99),
      July: Math.floor(50 + Math.random() * 99),
      August: Math.floor(50 + Math.random() * 99),
      September: Math.floor(50 + Math.random() * 99),
      October: Math.floor(50 + Math.random() * 99),
      November: Math.floor(50 + Math.random() * 99),
      December: Math.floor(50 + Math.random() * 99),
    };
    const itemsCollection = this.afs.collection('attendance');
    itemsCollection.doc(email).set(data);
  }

  saveStudentMarks(email) {
    const data = {
      'subject:1': Math.floor(1 + Math.random() * 99),
      'subject:2': Math.floor(1 + Math.random() * 99),
      'subject:3': Math.floor(1 + Math.random() * 99),
      'subject:4': Math.floor(1 + Math.random() * 99),
      'subject:5': Math.floor(1 + Math.random() * 99),
      'practical:1': Math.floor(1 + Math.random() * 99),
      'practical:2': Math.floor(1 + Math.random() * 99),
      'practical:3': Math.floor(1 + Math.random() * 99),
      'practical:4': Math.floor(1 + Math.random() * 99),
      'practical:5': Math.floor(1 + Math.random() * 99)
    };
    const itemsCollection = this.afs.collection('marks');
    itemsCollection.doc(email).set(data);
  }


}
