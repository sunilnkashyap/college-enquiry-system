import { Component, OnInit, OnChanges } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnChanges {

  email;
  password;
  message;

  constructor(public afAuth: AngularFireAuth, public router: Router) {
    this.message = '';
  }

  ngOnChanges() {
    console.log(this.message);
  }
  ngOnInit() {
  }

  loginEmail() {
    console.log('login email', this.email, this.password);
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password).then(x => {
      this.router.navigateByUrl('dashboard');
    }).catch(e => {
      console.log(e.message);
      this.message = e.message;
    });
  }

  loginFacebook() {
    console.log('login facebook');
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider()).then(x => {
      console.log(x);
    }).catch(e => {
      console.log(e.message);
      this.message = e.message;
    });
  }

  loginGoogle() {
    console.log('login google');
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then(x => {
      this.router.navigateByUrl('dashboard');
    }).catch(e => {
      console.log(e.message);
      this.message = e.message;
    });
  }

}
