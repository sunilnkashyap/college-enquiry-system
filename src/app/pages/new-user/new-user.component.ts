import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';

interface User {
  name: string;
  email: string;
  role: string;
  user_id: string;
}

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {


  private itemsCollection: AngularFirestoreCollection<User>;


  name;
  email;
  user_id;
  password;
  role;

  msg;

  editEmail;

  constructor(public route: ActivatedRoute, public afAuth: AngularFireAuth, public router: Router, private afs: AngularFirestore) {


    this.msg = '';

    this.email = '';
    this.password = '';
    this.user_id = '';
    this.role = '';
    this.name = '';


  }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('email')) {
      const email = this.route.snapshot.paramMap.get('email');

      this.editEmail = email;

      const data = this.afs.collection('users').doc(email).valueChanges().subscribe((x: any) => {
        this.email = x.email;
        this.name = x.name;
        this.role = x.role;
        this.user_id = x.user_id;
      });
      console.log(data);

    }
  }

  onSave() {

    if (this.email === '' || this.name === '' || this.role === '' || this.password === '') {
      this.msg = 'All fields are required';
      return false;
    }
    if (this.editEmail && this.editEmail !== '') {
      this.itemsCollection = this.afs.collection('users');
      const userData: User = {
        email: this.email,
        name: this.name,
        role: this.role,
        user_id: this.user_id
      };
      this.itemsCollection.doc(this.editEmail).update(userData);

      this.router.navigateByUrl('users/list');
    } else {
      this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password).then(x => {
        console.log(x);
        const userData: User = {
          email: this.email,
          name: this.name,
          role: this.role,
          user_id: this.user_id
        };

        this.itemsCollection = this.afs.collection('users');
        this.itemsCollection.doc(this.email).set(userData);
        this.router.navigateByUrl('users/list');
      }).catch(e => {
        console.log(e);
        alert(e.message);
        this.msg = e.message;
      });
    }


  }
  onClear() {
    this.name = '';
    this.email = '';
    this.password = '';
    this.role = '';
    this.user_id = '';
  }

}
