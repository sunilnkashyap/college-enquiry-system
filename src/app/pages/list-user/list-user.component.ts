import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {

  users;
  constructor(public afAuth: AngularFireAuth, public router: Router, private afs: AngularFirestore) {

  }

  ngOnInit() {
    this.afAuth.user.subscribe(x => {
      console.log(x);
    });

    this.users = this.afs.collection('users').valueChanges();

    console.log(this.users);
  }

  onDelete(email) {
    this.afs.collection('users').doc(email).delete();
    console.log(email);
    this.ngOnInit();
  }

}
