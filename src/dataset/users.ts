
export let students = [
  {
    name: 'Aman Bhardwaj',
    mobile: '9876543210',
    email: 'amanbharadwaj@gmail.com',
    department: 'Electronics',
  },
  {
    name: 'Kuchik Ashwini Shantaram',
    mobile: '9876543211',
    email: 'kuchik@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Heramb Devbhankar',
    mobile: '9876543212',
    email: 'heramb@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Vaibhav Dang',
    mobile: '9876543213',
    email: 'vaiabhav@gmail.com',
    department: 'Electronics & Communication',
  },
  {
    name: 'Anson Antony',
    mobile: '9876543214',
    email: 'anson@gmail.com',
    department: 'Civil',
  },
  {
    name: 'Gayatri Deore',
    mobile: '9876543215',
    email: 'gayatri@gmail.com',
    department: 'Mechanical',
  },
  {
    name: 'Shivam Anurag',
    mobile: '9876543216',
    email: 'shivam@gmail.com',
    department: 'Architecture',
  },
  {
    name: 'Pandhare Arti Prakash',
    mobile: '9876543217',
    email: 'pandhare@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Parade Rani Gajanan',
    mobile: '9876543218',
    email: 'parade@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Anand Ganesh',
    mobile: '9876543219',
    email: 'anand@gmail.com',
    department: 'Civil',
  },
]




export const teacher = [
  {
    name: 'Komal Bidkar',
    mobile: '9876543210',
    email: 'komal_teacher@gmail.com',
    department: 'Electronics',
  },
  {
    name: 'Budar Amit',
    mobile: '9876543211',
    email: 'amit_teacher@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Smita Patil',
    mobile: '9876543212',
    email: 'smita_teacher@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Nidhi Sinha',
    mobile: '9876543213',
    email: 'nidhi_teacher@gmail.com',
    department: 'Electronics & Communication',
  },
  {
    name: 'Rohit Pende',
    mobile: '9876543214',
    email: 'rohit_teacher@gmail.com',
    department: 'Civil',
  },
  {
    name: 'Chetna Jeswani',
    mobile: '9876543215',
    email: 'chetna_teacher@gmail.com',
    department: 'Mechanical',
  },
  {
    name: 'Akash Bhandari',
    mobile: '9876543216',
    email: 'akash_teacher@gmail.com',
    department: 'Architecture',
  },
  {
    name: 'Atharva Vaidya',
    mobile: '9876543217',
    email: 'atharva_teacher@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Himanshu Jotania',
    mobile: '9876543218',
    email: 'himanshu_teacher@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Dinesh Khoja',
    mobile: '9876543219',
    email: 'dinesh_teacher@gmail.com',
    department: 'Civil',
  },
]



export const staff = [
  {
    name: 'Monal Jangale',
    mobile: '9876543210',
    email: 'monal_staff@gmail.com',
    department: 'Electronics',
  },
  {
    name: 'Sarthak Burghate',
    mobile: '9876543211',
    email: 'sarthak@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Ankit Wardhan',
    mobile: '9876543212',
    email: 'ankit@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Sakina Sadaf',
    mobile: '9876543213',
    email: 'sakina@gmail.com',
    department: 'Electronics & Communication',
  },
  {
    name: 'Shweta Birajdar',
    mobile: '9876543214',
    email: 'shweta@gmail.com',
    department: 'Civil',
  },
  {
    name: 'Kajal Gaikwad',
    mobile: '9876543215',
    email: 'kajal@gmail.com',
    department: 'Mechanical',
  },
  {
    name: 'Saurav Jaiswal',
    mobile: '9876543216',
    email: 'saurav@gmail.com',
    department: 'Architecture',
  },
  {
    name: 'Dheeraj Verma',
    mobile: '9876543217',
    email: 'dheeraj@gmail.com',
    department: 'Information Technology',
  },
  {
    name: 'Dhabekar Ankita Anil',
    mobile: '9876543218',
    email: 'dhabekar@gmail.com',
    department: 'Computer Science',
  },
  {
    name: 'Pranil Farare',
    mobile: '9876543219',
    email: 'pranil@gmail.com',
    department: 'Civil',
  },
]
